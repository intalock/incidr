# incidr: A Custom Splunk Command for Filtering IP Addresses #
Either whitelisting or blacklisting IP addresses based on lookup table can be really frustrating. At first, you wouldn't mind specifying a handful of IPs in a lookup table to filter out events that shouldn't included in, say eventtypes or alerts. But the time will eventually come when there will be a need to white/blacklist a huge range of IP addresses, which can be impractical to list all in a lookup table.  

Incidr behaves in such a way that it **_filters out matching events_**, meaning events that have IP addresses that are within the CIDR block provided will be removed. However, the parameter `remove`, when set to false, will do the reverse.  

This is similar to Splunk's cidrmatch() function but can accept multiple CIDRs.
  
To use the custom command, deploy this Splunk app on your Search Head cluster (or standalone Splunk environment). The Incidr app utilizes the `cidr2regex.py` Python script by Tom Knight [check original code here](https://gist.github.com/tom-knight/1b5e0dcf39062af8910e). Having the Splunk Python SDK on the Splunk node(s) is prerequisite.  

## Deployment Guide ##
1. Clone this repository
2. Change ownership to splunk
3. Install the incidr app directory to $SPLUNK_HOME/etc/apps (via Deployer, if clustered)
4. It might be required for you to include the Splunk Python SDK in the $SPLUNK_HOME/etc/apps/incidr/bin directory
5. (Optional but recommended) Restart Splunk

## Structure ##
- Name of the custom command: `incidr`
- Paramters:
	- `cidr`
		- Desired CIDR block string (separate by comma for multiple CIDR)
		- Required: Yes
		- Type: String
		- Value: A valid CIDR block, e.g. "10.0.0.0/13"
	- `field`
		- IP Address containing field in the event results
		- Required: Yes
		- Type: Fieldname
		- Value: An existing, valid name of a field, e.g. "src_ip"
	- `remove`
		- Filters out the event when input matches the CIDR block as provided
		- Required: No
		- Type: Boolean
		- Value: `true` or `false`
	- `show_regex`
		- Displays the resulting regular expression pattern in a separate field
		- Required: No
		- Type: Boolean
		- Value: `true` or `false`
	- `regex_fieldname`
		- Assigns name to the field for containing the corresponding Regex result as displayed when `show_regex` is set to True
		- Required: No
		- Type: String
		- Value: "regex_pattern"

## Usage ##
To use the custom command, it is required to have pre-search in place since this is a streaming command. For example, your search might be  
```
host="10.0.100.15" OR index="cisco"  
```  
  
It is expected that the search will yield a field containing IP addresses like `src_ip` or the likes. Pipe the command to your search:  
```
host="10.0.100.15" OR index="cisco"   
| incidr cidr="10.0.0.0/24" field="src_ip"  
``` 

Above search will filter out or *remove* events containing src_ip that matches the regex pattern `^10.0.0.\d+$`. To revert the process, i.e. *retain* events that are in CIDR block provided, set the paramter `remove` to `false`:  
```
host="10.0.100.15" OR index="cisco"  
| incidr cidr="10.0.0.0/24" field="src_ip" remove=false  
```

Lastly, the app can also show the regex pattern in a separate field by setting the parameter `show_regex` to `true`:  
```
host="10.0.100.15" OR index="cisco"  
| incidr cidr="10.0.0.0/24" field="src_ip" remove=false show_regex=true  
```

##### Multiple CIDR ######
You can also use multiple CIDR using comma separation, such as:  

```
host="10.0.100.15" OR index="cisco"  
| incidr cidr="10.0.0.0/24,192.168.100.0/24" field="src_ip"  
```
How to use your list of CIDR blocks in a lookup table:  

```
host="10.0.100.15" OR index="cisco"  
| incidr field="src_ip" 
    [|inputlookup mycidrlookup.csv
	| stats values(CIDR_COLUMN) as cidr
	| rex field="cidr" mode=sed "s/\s/,/g"
	| eval retval = "cidr=\"" . cidr . "\""
	| return $retval]
```

##### Sample #####
```
| makeresults 
| eval src_ip = "192.168.0.0,10.0.0.15,10.0.0.15,10.0.0.16,10.0.0.17,10.0.0.18,10.0.0.19,10.0.0.20,192.168.0.4,192.168.0.2,192.168.0.3,192.168.0.5" 
| makemv delim="," src_ip 
| mvexpand src_ip
| incidr cidr="10.0.0.0/24" field="src_ip"
```

## Here's How the Original Script Works ##
The script accepts one argument that is a valid CIDR block, e.g. 10.0.0.0/24. It then follows the CIDR logic or rule to craft a regular-expression pattern as output. For example, an input of 

>52.58.100/13  

will result to a Regex pattern 
>^52.(5[6-9]|6[0-3]).\d+.\d+$

In the actual script. The usage can be done on *nix like this:

```
user@mymachine:~/dev/python$ echo "52.58.100/13" | python cidr2regex.py  
^52.(5[6-9]|6[0-3]).\d+.\d+$  
```
  
 
### Authorhip ###
##### Original Python Script Sources #####
**Mordy Ovits** [profile](https://gist.github.com/mordyovits) | [source code](https://gist.github.com/mordyovits/ab2ba4635a1ed9d2065b)  
**Tom Knight** [profile](https://gist.github.com/tom-knight) | [source code](https://gist.github.com/tom-knight/1b5e0dcf39062af8910e)  
**Lowell Alleman** [profile](https://gist.github.com/lowell80) | [source code](https://gist.github.com/lowell80/10428118)  


##### This Splunk App Implementation: #####
Daniel L. Astillero  
Contact: daniel.astillero@intalock.com // daniel.l.astillero@gmail.com  
Association: Intalock Technologies (https://www.intalock.com.au)
